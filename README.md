# Nim Slack Client

## Introduction
Provides a nim wrapper to Slack's Real Time Messaging API

A slack bot token is required to access the API
=======
# Slack API for Nim

## Instructions
Add your token to your configDir/token.cfg. It should look like this:
```
{
    "token": "xxxxxxx"
}
```
